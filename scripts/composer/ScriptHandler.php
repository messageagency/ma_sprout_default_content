<?php

namespace MaSproutDefaultContent\composer;

use Composer\EventDispatcher\ScriptExecutionException;
use Composer\Script\Event;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Script handler for ma_sprout_default_content.
 */
class ScriptHandler {

  /**
   * Transform ma_sprout_default_content into custom module after creation.
   *
   * @param \Composer\Script\Event $event
   *   The event.
   */
  public static function transformProject(Event $event) {
    $package = $event->getComposer()->getPackage();
    // Validate package.
    if ($package->getName() != 'messageagency/ma_sprout_default_content') {
      return;
    }

    $path = static::getInstallPath();
    $path_parts = explode('/', $path);
    $name = str_replace('-', '_', end($path_parts));
    if (!preg_match("/^[a-z_][a-z0-9_]+$/", $name)) {
      $event->getComposer()->getPluginManager()->uninstallPackage($package);
      $event->getIO()->error("Unable to create module '$name'. Module names may include lowercase alphanumeric and underscore, and may not start with a number. Please remove the new project directory and try again.");
      throw new ScriptExecutionException();
    }

    // The create-project command drops the script into the package directory,
    // where we recursively perform replacements for our placeholders.
    static::doReplacements('./', $name);

    // Rename files.
    static::doRenames('./', $name);

  }

  /**
   * Helper to get created package's path.
   *
   * @return string
   *   The path to project getting installed.
   */
  protected static function getInstallPath() {
    // Seriously baffled there's no way to get raw command line args.
    $argv = $_SERVER['argv'];

    // Shift the first 2 args off, which are `composer` and `create-project`.
    array_shift($argv);
    array_shift($argv);
    foreach ($argv as $arg) {
      if (strpos($arg, '-') === 0) {
        continue;
      }
      // Always ignore these.
      if (strpos($arg, 'messageagency/ma_sprout_default_content') !== FALSE || $arg == 'create-project') {
        continue;
      }
      return $arg;
    }
    return NULL;
  }

  /**
   * Replace placeholders in theme, recursively.
   *
   * @param string $path
   *   Path to theme.
   * @param string $name
   *   Machine name of theme.
   */
  protected static function doReplacements($path, $name) {
    // Replace module name with theme name.
    // This is, obviously, a bit rickety.
    $name = str_replace(["_module", "_content", "_default_content"], "", $name);
    $alterations = [
      'SITE_THEME_NAME' => ucwords(str_replace('_', ' ', $name)),
      'SITE_THEME_MACHINE_NAME' => $name,
      "// @codingStandardsIgnoreStart\n" => '',
      "// @codingStandardsIgnoreEnd\n" => '',
    ];

    $finder = Finder::create();
    $finder->files()->in($path);
    foreach ($finder as $file) {
      if ($file->isDir()) {
        static::doReplacements($file->getPath(), $name);
      }
      else {
        $file_contents = $file->getContents();
        $file_contents = str_replace(array_keys($alterations), $alterations, $file_contents);
        file_put_contents($file->getPathname(), $file_contents);
      }
    }
  }

  /**
   * Rename files per new project name.
   *
   * @param string $path
   *   Path to theme.
   * @param string $name
   *   Machine name of theme.
   */
  protected static function doRenames($path, $name) {
    $finder = Finder::create();
    $fs = new Filesystem();
    $finder->files()->in($path);
    foreach ($finder as $file) {
      if ($file->isDir()) {
        static::doRenames($file->getPath(), $name);
      }
      else {
        if (strpos($file->getBasename(), 'ma_sprout_default_content') !== FALSE) {
          $file_new_path = str_replace('ma_sprout_default_content', $name, $file->getPathname());
          $fs->rename($file->getPathname(), $file_new_path);
        }
      }
    }
  }

}
