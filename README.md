# SITE_THEME_NAME Content

Enabling this module will create content from the yml files in the `/content`
directory.

Content can be re-exported from any site with this content by running 
`drush dcem SITE_THEME_MACHINE_NAME_content`.

The list of uuids to export is found in 
`SITE_THEME_MACHINE_NAME_content.info.yml`.
